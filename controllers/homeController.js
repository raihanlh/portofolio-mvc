const { Portofolio } = require('../models');
const fs = require('fs');
module.exports = {
    index: (req, res) => {
        let { about } = JSON.parse(fs.readFileSync('./config/static.json'));

        Portofolio.findAll()
        .then(portofolios => {
            res.render('./home', {
                portofolios, title: "Raihan Luthfi Haryawan", about
            });
        })
        .catch(err => {
            res.status(404).json({ 
                status: 404,
                message: err.message 
            });
        });
    }
}