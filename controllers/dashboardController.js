const { Portofolio } = require('../models');
const fs = require('fs');

module.exports = {
    index: (req, res) => {
        let { about } = JSON.parse(fs.readFileSync('./config/static.json'));

        Portofolio.findAll()
        .then(portofolios => {
            res.render('./dashboard', {
                portofolios, about, title: "Dashboard"
            });
        })
        .catch(err => {
            res.status(404).json({ 
                status: 404,
                message: err.message 
            });
        });
    },
    portofolio: (req, res) => {
        Portofolio.findAll()
        .then(portofolios => {
            res.render('./dashboard/portofolio', {
                portofolios, title: "Dashboard | Portofolio"
            });
        })
        .catch(err => {
            res.status(404).json({ 
                status: 404,
                message: err.message 
            });
        });
    }
}