const fs = require('fs');

module.exports = {
    update: (req, res) => {
        let staticData = JSON.parse(fs.readFileSync('./config/static.json'));

        staticData.about = req.body.about;

        fs.writeFileSync('./config/static.json', JSON.stringify(staticData));

        res.status(200).json({
            status: 200,
            message: "Berhasil mengganti introduction",
            data: req.body.about
        })
    }
}