const { User } = require('../../models');
const jwt = require('../../lib/jwt');

module.exports = {
    // Create User API: POST /api/v1/user/register
    register: async (req, res) => {
        try {
            const { username, password } = req.body;
            encryptedPassword = jwt.encrypt(password, 10);
            const user = await User.create({ username, password: encryptedPassword });

            res.status(200).json({
                status: 200,
                message: "User berhasil didaftarkan",
                data: user
            })
        }
        catch(err) {
            res.status(400).json({
                status: 400,
                message: err.message
            });
        }
    },
    // Login User API: POST /api/v1/user/login
    login: async (req, res) => {
        try {
            const { username, password } = req.body;
            const user = await User.findOne({ where: { username } });
            if (!user) {
                res.json({ status: 404, message: "User not found" });
            } 
    
            const isPasswordValid = jwt.checkPassword(password, user.password);
            if (!isPasswordValid) {
                res.json({ status: 404, message: "Invalid password" });
            }
            const token = jwt.generateToken(user.id, user.username);

            const options = {
                maxAge: 1000 * 60 * 50, // would expire after 1 hour
                httpOnly: true, // The cookie only accessible by the web server
            }
            
            res.cookie('token', token, options)

            res.json({
                id: user.id,
                username: user.username,
                token: token
            });
        }
        catch(err) {
            res.status(400).json({
                message: err.message
            });
        }
    },
    // Logout User API: POST /api/v1/user/logout
    logout: (req, res) => {
        res.clearCookie('token');
        res.send('Logout succesfull');
    }
}