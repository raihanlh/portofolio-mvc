const { Portofolio } = require('../../models');
const fs = require('fs');

module.exports = {
    // Create API: POST /api/v1/portofolio/create
    create: (req, res) => {
        const {
            name,
            link
        } = req.body;

        if (!req.files || Object.keys(req.files).length === 0) {
            return res.status(400).send('No files were uploaded.');
        }

        let thumbnail = req.files.thumbnail;
        let thumbnail_url = `/assets/img/${Date.now()}-${req.files.thumbnail.name}`;

        let imgPath = process.cwd() + '/public' + thumbnail_url

        thumbnail.mv(imgPath, (err) => {
            if (err)
              return res.status(500).send(err);
        });

        Portofolio.create({
            name,
            link,
            thumbnail_url
        })
        .then(portofolio => {
            res.status(201).json({
                "status": 201,
                "message": "Portofolio berhasil dibuat",
                "data": portofolio
            });
        })
        .catch(err => {
            console.log(err);
            res.status(422).json("Can't create portofolio");
        });
    },
    // Get by id API: GET /api/v1/portofolio/:id 
    getById: (req, res) => {
        Portofolio.findOne({ where: { id: req.params.id } })
        .then(portofolio => res.status(201).json({
            "status": 201,
            "message": "Artikel berhasil diambil",
            "data": portofolio
        })).catch(err => {
            console.log(err);
            res.status(422).json("Failed to fetch portofolio");
        });
    },
    // Get all: GET /api/v1/portofolio/
    getAll: (req, res) => {
        Portofolio.findAll()
        .then(portofolios => res.status(201).json({
            "status": 201,
            "message": "Artikel berhasil diambil",
            "data": portofolios
        })).catch(err => {
            console.log(err);
            res.status(422).json("Failed to fetch portofolio");
        });
    },
    // Update API: /api/v1/portofolio/update/:id
    update: (req, res) => {
        const {
            name,
            link
        } = req.body;

        if (!req.files || Object.keys(req.files).length === 0) {
            return res.status(400).send('No thumbnail were uploaded.');
        }

        let thumbnail = req.files.thumbnail;
        let thumbnail_url = `/assets/img/${Date.now()}-${req.files.thumbnail.name}`;

        let imgPath = process.cwd() + '/public' + thumbnail_url

        thumbnail.mv(imgPath, (err) => {
            if (err)
              return res.status(500).send(err);
        });

        Portofolio.update({
            name,
            link,
            thumbnail_url
        }, { where: { id: req.params.id } })
        .then(portofolio => {
            res.status(201).json({
                "status": 201,
                "message": "Portofolio berhasil diubah",
                "data": portofolio
            });
        })
        .catch(err => {
            console.log(err);
            res.status(422).json("Can't create portofolio");
        });
    },
    // Delete API:
    delete: (req, res) => {
        const query = { where: { id: req.params.id } };

        Portofolio.findOne(query)
        .then((portofolio) => {
            let imgPath = process.cwd() + '/public' + portofolio.thumbnail_url;
            Portofolio.destroy(query)
            .then(() => {
                fs.unlinkSync(imgPath);
    
                res.status(201).json({
                    "status": 201,
                    "message": "Portofolio berhasil dihapus",
                    "data": portofolio
                })}
            )
            .catch(err => {
                console.log(err);
                res.status(422).json({
                    "message": "Gagal menghapus portofolio",
                    "err": err
                });
            });
        })
    }
}