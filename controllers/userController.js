const { User } = require('../models');
const jwt = require('../lib/jwt');

module.exports = {
    register: (req, res) => {
        res.render('./user/register', { title: "Register" })
    },
    login: (req, res) => {
        res.render('./user/login', { title: "Login", message: "" });
    },
    doRegister: async (req, res) => {
        try {
            const { username, password } = req.body;
            encryptedPassword = jwt.encrypt(password, 10);
            const user = await User.create({ username, password: encryptedPassword });
            res.redirect('/user/login');
        }
        catch(err) {
            res.redirect('/user/register');
        }
    },
    doLogin: async (req, res) => {
        try {
            const { username, password } = req.body;
            const user = await User.findOne({ where: { username } });
            if (!user) {
                return res.render('./user/login', { message: "User not found" })
            } 
    
            const isPasswordValid = jwt.checkPassword(password, user.password);
            if (!isPasswordValid) {
                return res.render('./user/login', { message: "Invalid password" })
            }
            const token = jwt.generateToken(user.id, user.username);

            const options = {
                maxAge: 1000 * 60 * 60, // would expire after 60 minutes
                httpOnly: true, // The cookie only accessible by the web server
            }
            
            res.cookie('token', token, options)
            res.redirect('/dashboard')
        }
        catch(err) {
            res.status(400).json({
                message: err.message
            });
        }
    },
    logout: (req, res) => {
        res.clearCookie('token');
        res.redirect('/user/login');
    }
}