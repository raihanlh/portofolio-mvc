const { Portofolio } = require('../models');
const fs = require('fs');

module.exports = {
    // GET /portofolio
    index: (req, res) => {
        Portofolio.findAll().then(portofolios => {
            res.render('./portofolios/index', {
                portofolios
            });
        });
    },
    // GET /portofolio/create
    createPortofolio: (req, res) => {
        res.render("./portofolio/create", { isAuth: req.isAuthenticated() });
    },
    // GET /portofolio/:id
    detail: (req, res) => {
        // Not implemented
    },
    // POST /portofolio/create
    create: (req, res) => {
        const {
            name,
            link
        } = req.body;

        if (!req.files || Object.keys(req.files).length === 0) {
            return res.status(400).send('No files were uploaded.');
        }

        let thumbnail = req.files.thumbnail;
        let thumbnail_url = `/assets/img/${Date.now()}-${req.files.thumbnail.name}`;

        let imgPath = process.cwd() + '/public' + thumbnail_url

        thumbnail.mv(imgPath, (err) => {
            if (err)
              return res.status(500).send(err);
        });

        Portofolio.create({
            name,
            link,
            thumbnail_url
        })
        .then(portofolio => {
            res.redirect('/dashboard');
        })
        .catch(err => {
            console.log(err);
            res.status(422).json("Can't create portofolio");
        });
    },
    // GET /portofolio/:id
    show: (req, res) => {
        Portofolio.findOne({ where: { id: req.params.id } })
        .then(portofolio => res.status(201).json({
            "status": 201,
            "message": "Artikel berhasil diambil",
            "data": portofolio
        })).catch(err => {
            console.log(err);
            res.status(422).json("Failed to fetch portofolio");
        });
    },
    update: async (req, res) => {
        const { name, link } = await Portofolio.findOne({ where: { id: req.params.id } })
        res.render('./portofolio/update', { id: req.params.id, name, link })
    },
    // POST /portofolio/update/:id
    doUpdate: (req, res) => {
        const {
            name,
            link
        } = req.body;

        if (!req.files || Object.keys(req.files).length === 0) {
            return res.status(400).send('No thumbnail were uploaded.');
        }

        let thumbnail = req.files.thumbnail;
        let thumbnail_url = `/assets/img/${Date.now()}-${req.files.thumbnail.name}`;

        let imgPath = process.cwd() + '/public' + thumbnail_url

        thumbnail.mv(imgPath, (err) => {
            if (err)
              return res.status(500).send(err);
        });

        Portofolio.update({
            name,
            link,
            thumbnail_url
        }, { where: { id: req.params.id } })
        .then(portofolio => {
            res.redirect('/dashboard');
        })
        .catch(err => {
            console.log(err);
            res.status(422).json("Can't create portofolio");
        });
    },
    // DELETE /portofolio/delete/:id
    delete: (req, res) => {
        const query = { where: { id: req.params.id } };

        Portofolio.findOne(query)
        .then((portofolio) => {
            let imgPath = process.cwd() + '/public' + portofolio.thumbnail_url;
            Portofolio.destroy(query)
            .then(() => {
                fs.unlinkSync(imgPath);
    
                res.status(201).json({
                    "status": 201,
                    "message": "Portofolio berhasil dihapus",
                    "data": portofolio
                })}
            )
            .catch(err => {
                console.log(err);
                res.status(422).json({
                    "message": "Gagal menghapus portofolio",
                    "err": err
                });
            });
        })
    }
}