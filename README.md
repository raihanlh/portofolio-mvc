## Install Modules

```
npm install
```

## Create .env

```
touch .env
```

## Add your secret key in .env

```
SECRET_KEY="YOURSECRETKEY"
```