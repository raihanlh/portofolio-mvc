const router = require('express').Router();
const portofolioController = require('../controllers/portofolioController');

router.get('/', portofolioController.index);

router.get('/create', portofolioController.createPortofolio);
router.post('/create', portofolioController.create);

// router.get('/detail/:id', portofolioController.detail);
// router.get('/:id', portofolioController.show);

router.get('/update/:id', portofolioController.update);
router.post('/update/:id', portofolioController.doUpdate);
router.delete('/delete/:id', portofolioController.delete);

module.exports = router;