const router = require('express').Router();
const userServiceController = require('../../controllers/api/userServiceController');

router.post('/register', userServiceController.register);
router.post('/login', userServiceController.login);
router.post('/logout', userServiceController.logout);

module.exports = router;
