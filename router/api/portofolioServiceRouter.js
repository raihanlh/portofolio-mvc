const router = require('express').Router();
const portofolioServiceControlller = require('../../controllers/api/portofolioServiceControlller');

router.get('/', portofolioServiceControlller.getAll);
router.post('/create', portofolioServiceControlller.create);
router.get('/:id', portofolioServiceControlller.getById);
router.put('/update/:id', portofolioServiceControlller.update);
router.delete('/delete/:id', portofolioServiceControlller.delete);

module.exports = router;