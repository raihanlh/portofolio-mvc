const router = require('express').Router();
const aboutController = require('../controllers/aboutController');

router.put('/update', aboutController.update);

module.exports = router;