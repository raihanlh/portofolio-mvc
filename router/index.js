const router = require('express').Router();
const homeRouter = require('./homeRouter');
const userRouter = require('./userRouter');
const portofolioRouter = require('./portofolioRouter');
const dashboardRouter = require('./dashboardRouter');
const aboutRouter = require('./aboutRouter');

const portofolioServiceRouter = require('./api/portofolioServiceRouter');
const userServiceRouter = require('./api/userServiceRouter');

const auth = require('../middlewares/auth');

router.use('/', homeRouter);
router.use('/user', userRouter);
router.use('/portofolio', auth, portofolioRouter);
router.use('/dashboard', auth, dashboardRouter);
router.use('/about', aboutRouter);

router.use('/api/v1/portofolio', portofolioServiceRouter);
router.use('/api/v1/user', userServiceRouter);

module.exports = router;