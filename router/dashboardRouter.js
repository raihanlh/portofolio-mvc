const router = require('express').Router();
const dashboardController = require('../controllers/dashboardController');

router.get('/', dashboardController.index);
router.get('/portofolio', dashboardController.portofolio);

module.exports = router;